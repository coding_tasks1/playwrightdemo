using PWrightDemo.Pages;

namespace PWrightDemo.Tests;

[Parallelizable(ParallelScope.Self)]
[TestFixture]
public class BasePageTest : PageTest
{
    public BasePage _basePage;

    public BasePageTest() { }

    [SetUp]
    public async Task SetUp()
    {
        _basePage = new BasePage(await Browser.NewPageAsync());
        await _basePage.Page.GotoAsync("https://preview.keenthemes.com/");

    }

    [Test]
    public async Task MainNavigationAsync()
    {
        // Assertions use the expect API.
        await Expect(_basePage.Page).ToHaveURLAsync("https://preview.keenthemes.com/metronic8/demo1/index.html");
    }

    
    [TestCase("https://preview.keenthemes.com/metronic8/demo1/index.html")]
    public async Task UrlAsync(string url)
    {
        await _basePage.Page.GotoAsync(url);
        // Assertions use the expect API.
        await Expect(_basePage.Page).ToHaveURLAsync(url);
        
    }

}