﻿using Microsoft.Playwright;
using PWrightDemo.Pages;
using PWrightDemo.Utils.PageComponentModels;

namespace PWrightDemo.Tests
{
    [Parallelizable(ParallelScope.Self)]
    [TestFixture]
    public class GeneralCustomerListingPageTest : BasePageTest
    {
        public GeneralCustomerListingPage _gclPage;

        public GeneralCustomerListingPageTest() : base() { }

        [SetUp]
        public void SetUpDerived()
        {          
            _gclPage = new GeneralCustomerListingPage(base._basePage.Page);
        }

        [Test]
        public async Task GetDataTableValues()
        {  
            await UrlAsync("https://preview.keenthemes.com/craft/apps/customers/list.html");
            var rows = await _gclPage.GetTableRowsAsync("table#kt_customers_table tbody tr");

            Assert.IsNotNull(rows, "The rows collection should not be null");
            Assert.IsTrue(rows.Any(), "The rows collection should contain at least one element");
            
            List<GeneralCustomerListingTable?> rowObjects = await _gclPage
                .ExtractDataTableRowsAsObjects<GeneralCustomerListingTable>();            
        }
    }
}
