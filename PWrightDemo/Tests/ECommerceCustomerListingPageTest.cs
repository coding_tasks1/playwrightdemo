﻿using Microsoft.Playwright;
using PWrightDemo.Pages;
using PWrightDemo.Utils.PageComponentModels;

namespace PWrightDemo.Tests
{
    [Parallelizable(ParallelScope.Self)]
    [TestFixture]
    public class ECommerceCustomerListingPageTest : BasePageTest
    {
        public ECommerceCustomerListingPage _eclPage;        

        public ECommerceCustomerListingPageTest() : base() { }

        [SetUp]
        public void SetUpDerived()
        {           
            _eclPage = new ECommerceCustomerListingPage(base._basePage.Page);
        }

        [Test]
        public async Task GetDataTableValuesAsync()
        {  
            await UrlAsync("https://preview.keenthemes.com/craft/apps/ecommerce/customers/listing.html");
            var rows = await _eclPage.GetTableRowsAsync("table#kt_customers_table tbody tr");

            Assert.IsNotNull(rows, "The rows collection should not be null");
            Assert.IsTrue(rows.Any(), "The rows collection should contain at least one element");

            List<ECommerceCustomerListingTable?> tableObjects = await _eclPage
                .ExtractDataTableRowsAsObjects<ECommerceCustomerListingTable>();            
        }

    }

}
