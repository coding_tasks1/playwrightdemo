﻿namespace PWrightDemo.Utils.PageComponentModels
{
    public class ECommerceCustomerListingTable : DataTable
    {
        public string? ip { get; set; }

        public override ECommerceCustomerListingTable ExtractValuesFromRowText(IList<string> fieldsList)
        {
            // Extract values and set them to object properties
            if (fieldsList.Count >= 4) // Assuming at least three values are present
            {
                customerName = fieldsList[1].Trim();
                email = fieldsList[2].Trim();
                ip = fieldsList[4].Trim();

                // You can continue to extract and set other properties based on your data
            }

            return this;
        }
    }
}
