﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PWrightDemo.Utils.PageComponentModels
{
    public abstract class DataTable
    {
        public string? customerName { get; set; }
        public string? email { get; set; }

        public abstract DataTable ExtractValuesFromRowText(IList<string> fieldsList);
    }
}
