﻿using Microsoft.Playwright;
using PWrightDemo.Utils.PageComponentModels;

namespace PWrightDemo.Pages
{
    public class GeneralCustomerListingPage : BasePage
    {
        public GeneralCustomerListingPage(IPage page) : base(page) { }

        // other functions specific to the page can be defined here 
    }
}
