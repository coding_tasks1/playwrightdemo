﻿using Microsoft.Playwright;
using PWrightDemo.Utils.PageComponentModels;

namespace PWrightDemo.Pages
{
    public class BasePage 
    {
        public readonly IPage Page;
        public IReadOnlyList<ILocator>? dataTableRows { get; set; }

        public BasePage(IPage page)
        {
            Page = page ?? throw new ArgumentNullException(nameof(page));
        }

        /*
         * By making the table identifier a paramter this method can be used to extract rows from different tables
         */
        public async Task<IReadOnlyList<ILocator>?> GetTableRowsAsync(string identifier)
        {
            if (Page != null)
            {
                dataTableRows = await Page.Locator(identifier).AllAsync();
            }
            return dataTableRows;
        }

        /*
         * By using the generic type T, this method can be used to return different types 
         * like ECommerceCustomerListingTable or GeneralCustomerListingTable
         * as required by the page being tested
         */
        public async Task<List<T?>> ExtractDataTableRowsAsObjects<T>() where T : DataTable, new()
        {            
            List<T?> tableRowObjects = new List<T?>();

            for (int i = 0; i < dataTableRows?.Count; i++)
            {
                try
                {
                    var rowText = await dataTableRows[i].Locator("td").AllInnerTextsAsync();
                    var RowObject = new T().ExtractValuesFromRowText((IList<string>)rowText);
                    tableRowObjects.Add((T?)RowObject);
                }
                catch (Exception ex)
                {
                    // Handle or log the exception
                    Console.WriteLine($"Error processing row {i}: {ex.Message}");
                }
            }

            return tableRowObjects;
        }

    }
}
